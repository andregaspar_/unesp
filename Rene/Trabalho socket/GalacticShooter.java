package galacticshooter;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.Timer;

public class GalacticShooter {

    public static void main(String[] args) {        
            Jogo a = new Jogo();
            a.init();                    
    }
}

class nave {
    
    private JLabel j;
    private Boolean canShoot = true;

    public JLabel getJ() {
        return j;
    }

    public void setJ(JLabel j) {
        this.j = j;
    }

    public Boolean getCanShoot() {
        return canShoot;
    }

    public void setCanShoot(Boolean canShoot) {
        this.canShoot = canShoot;
    }
           
}


 class Jogo extends JFrame {

    private ArrayList<nave> naves = new ArrayList<>(2);
    public Boolean gaming = false;
    public Boolean delay = false;
    private static final ScheduledExecutorService WORKER
            = Executors.newSingleThreadScheduledExecutor();
    
    public Jogo() {
        this.set_configuracoes();
    }
    
    private void setFonte(JLabel label) {
        Font labelFont = label.getFont();
        String labelText = label.getText();
        int stringWidth = label.getFontMetrics(labelFont).stringWidth(labelText);
        int componentWidth = label.getWidth();

        double widthRatio = (double) componentWidth / (double) stringWidth;

        int newFontSize = (int) (labelFont.getSize() * widthRatio);
        int componentHeight = label.getHeight();

        int fontSizeToUse = Math.min(newFontSize, componentHeight);

        label.setFont(new Font(labelFont.getName(), Font.PLAIN, fontSizeToUse));
    }

    private void andar(int p, int l) {
        try {
            new ObjectOutputStream(conn.getOutputStream()).writeUTF(p+""+l);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        JLabel j = naves.get(p).getJ();
        Point loc = j.getLocation();
        if ((l == -1 && loc.x > 0) || (l == 1 && loc.x < 560)) {
            j.setLocation(loc.x + l * 5, loc.y);
        }
    }

    private void atirar(int p) {        
        if (naves.get(p).getCanShoot()) {
            try {
            new ObjectOutputStream(conn.getOutputStream()).writeUTF(p+""+0);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
            naves.get(p).setCanShoot(false);
            int aux = p == 0 ? 1 : -1;
            JLabel j = new JLabel(p == 0 ? "Â¡" : "!");
            j.setForeground(p == 0 ? Color.red : Color.blue);
            Point l = naves.get(p).getJ().getLocation();
            j.setSize(40, 40);
            j.setLocation(l.x, l.y + 11 * aux);
            setFonte(j);
            final JFrame f = this;
            this.add(j);
            Timer t = new Timer(2, new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (gaming) {
                        Point l2 = j.getLocation();
                        int p2 = p == 0 ? 1 : 0;
                        JLabel t = naves.get(p2).getJ();
                        Point lt = t.getLocation();
                        if (l2.x >= lt.x && l2.x <= (lt.x + 30)) {
                            if ((p == 0 && l2.y >= lt.y) || (p == 1 && l2.y <= lt.y + 30)) {
                                morrer(p);
                                return;
                            }
                        }
                        if (l2.y < 11 || l2.y > 319) {
                            f.getContentPane().remove(j);
                            f.getContentPane().repaint();
                            ((Timer) e.getSource()).stop();
                            return;
                        }
                        j.setLocation(l2.x, l2.y + 1 * aux);
                    } else {
                        f.getContentPane().remove(j);
                        f.getContentPane().repaint();
                        ((Timer) e.getSource()).stop();
                    }
                }
            });
            t.setRepeats(true);
            t.start();

            Timer tt = new Timer(500, new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    naves.get(p).setCanShoot(true);
                }
            });
            tt.setRepeats(false);
            tt.start();
        }
    }

    private void morrer(int p) {
        this.delay = true;
        this.gaming = false;
        this.getContentPane().remove(naves.get(p ^ 1).getJ());
        JLabel j = new JLabel("Player " + p + 1 + " ganhou!");
        j.setSize(200, 50);
        j.setForeground(p == 0 ? Color.red : Color.blue);
        j.setLocation(225, 175);
        setFonte(j);
        this.add(j);
        this.getContentPane().repaint();
        Runnable task = () -> {
            /* Do somethingâ€¦ */
            delay = false;
            init();
        };
        WORKER.schedule(task, 2, TimeUnit.SECONDS);
        try {
            conn.close();
        } catch (IOException ex) {
             ex.printStackTrace();
        }
    }

    public int i;

    Socket conn;
    private void connect()
    {
        //SocketAddress endpoint= new InetSocketAddress(tf.getText(), 6665);
        try {
            conn = new Socket(tf.getText(), 6665);
            conn.setKeepAlive(true);
            //  conn.connect(endpoint);            
            start();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    public void init() {
        this.getContentPane().removeAll();
        this.getContentPane().repaint();       
        JLabel c = new JLabel("Digite o ip:");

        c.setForeground(Color.red);
        c.setSize(200, 50);
        c.setLocation(260, 165);
        setFonte(c);
        if(tf==null)
        {
         tf = new JTextField("localhost");
        
        tf.setForeground(Color.red);
        tf.setSize(200,50);
        tf.setLocation(260,205);
        }
        this.add(c);
        this.add(tf);
        this.getContentPane().repaint();
        tf.requestFocus();        
        tf.addActionListener((e) -> {
            System.out.println("conectando");
            connect();
        });
    }
    JTextField tf;
    
    public void start() {
        delay = true;
        this.getContentPane().removeAll();
        this.getContentPane().repaint();
        JLabel c = new JLabel("3");
        
        //thread connect
        Thread cs = new Thread(()->{
            Socket socket = null;
            PrintStream os = null;
            Scanner is = null;
            try {
                socket = new Socket("127.0.0.1", 6665);
                is = new Scanner(socket.getInputStream());
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            
            String teste = is.nextLine();
            System.out.println(teste);
        });

        c.setForeground(Color.red);
        c.setSize(200, 50);
        c.setLocation(260, 165);
        setFonte(c);
        this.add(c);
        i = 3;
        final JFrame f = this;
        Timer t = new Timer(1000, (e)
                -> {
            if (i > 1) {
                c.setText("" + --i);
            } else {
                f.getContentPane().remove(c);
                f.getContentPane().repaint();
                gaming = true;
                ((Timer) e.getSource()).stop();
            }
        });
        t.setRepeats(true);
        t.start();

        Runnable task = ()-> {
            dothing();
        };
        WORKER.schedule(task, 3, TimeUnit.SECONDS);                      
    }

    private void dothing() {
        this.getContentPane().removeAll();
        naves.clear();
        JLabel j;
        nave n;

        j = new JLabel("\\/");
        j.setSize(40, 40);
        setFonte(j);
        j.setForeground(Color.red);
        j.setLocation(295, 1);

        this.add(j);
        n = new nave();
        n.setJ(j);
        naves.add(n);

        j = new JLabel("/\\");
        j.setForeground(Color.blue);
        j.setSize(40, 40);
        j.setLocation(295, 319);
        setFonte(j);
        this.add(j);
        n = new nave();
        n.setJ(j);
        naves.add(n);

        this.getContentPane().repaint();
        delay = false;
        this.requestFocus();
    }

    private void set_configuracoes() {
        this.getContentPane().setLayout(null);
        this.getContentPane().setBackground(Color.black);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        Dimension d = new Dimension(600, 400);
        this.setSize(d);
        this.setResizable(false);
        this.setVisible(true);
        this.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (!delay) {
                    if (gaming) {
                        switch (e.getKeyCode()) {
                            case KeyEvent.VK_LEFT:
                                andar(1, -1);
                                break;
                            case KeyEvent.VK_RIGHT:
                                andar(1, 1);
                                break;
                            case KeyEvent.VK_ENTER:
                                atirar(1);
                                break;
                            case KeyEvent.VK_A:
                                andar(0, -1);
                                break;
                            case KeyEvent.VK_D:
                                andar(0, 1);
                                break;
                            case KeyEvent.VK_SPACE:
                                atirar(0);
                                break;
                        }
                    }// else {
                      //  connect();
                    //}
                }
            }
        });
    }

}


