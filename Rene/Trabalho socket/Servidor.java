package javaapplication4;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor {

    public static void main(String[] args) {
        ServerSocket socket;
        int port = 6665;
        try{
            socket = new ServerSocket();
            socket.setReuseAddress(true);
            socket.bind(new InetSocketAddress(port));
        }catch(IOException e){
            System.out.println("Could not listen on port "+ port + ": " + e);
            return;
        }
        System.out.println("Succes");

        for (int i = 0; i < 2; i++) {
            Socket clientSocket = null;
            try {
                clientSocket = socket.accept();
            } catch (IOException e) {
                System.out.println("Accept failed: " + e);
                System.exit(1);
            }

            System.out.println("Accept success!");

            new Servindo(clientSocket).start();

        }

        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();

        }
    }
}

class Servindo extends Thread {

    String frase;
    Socket clientSocket;    
  
    Servindo(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }

    public void run() {
        try {
            try (DataInputStream is = new DataInputStream(new BufferedInputStream(clientSocket.getInputStream()));
                 PrintStream os = new PrintStream(new BufferedOutputStream(clientSocket.getOutputStream(), 1024), true))
            {
                String inputLine = " ";
                
                do {
                    inputLine = is.readLine();
                    //os.println((Math.abs(Integer.parseInt(inputLine.charAt(0)+"")-1))+""+inputLine.charAt(1));
                    os.println((Integer.parseInt(inputLine.charAt(0)+""))+""+inputLine.charAt(1));
                    os.flush();
                    
                } while (!inputLine.equals(""));
                
            }
            clientSocket.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
};
